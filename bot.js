require('dotenv').config()
const Discord = require('discord.js')
const logger = require('./src/utils/logger')

const client = new Discord.Client()
// Initialize and set up all of the commands
require('./src/commands')(client)

client.on('reconnecting', () => {
    logger.logWarn('Client is reconnecting...')
})

client.on('disconnect', () => {
    logger.logWarn('Client has disconnected!')
    if (process.env.ENV_NAME === 'prod') {
        client.login(process.env.DISCORD_CLIENT_TOKEN)
    }
})

client.on('resume', (missed) => {
    logger.logWarn(`Client has resumed connection.  ${missed} events were missed while offline...`)
})

module.exports = client
