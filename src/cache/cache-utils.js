const getValue = (cache, id) => {
    if (cache && cache.enabled) {
        return cache[id]
    }
    return null
}

const updateCacheValue = (cache, id, value) => {
    const updatedCache = cache
    if (id && value && updatedCache && updatedCache.enabled) {
        const newId = id.toLowerCase()
        updatedCache[newId] = value
    }
}
const clearCacheValue = (cache, id) => {
    const updatedCache = cache
    if (id) {
        const newId = id.toLowerCase()
        if (updatedCache && updatedCache.enabled && updatedCache[newId]) {
            delete updatedCache[newId]
        }
    }
}
module.exports = {
    getValue,
    updateCacheValue,
    clearCacheValue,
}
