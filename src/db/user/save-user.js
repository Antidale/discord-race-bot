const saveRowWithId = require('../util/save-row-with-id')
const userCache = require('../../cache/user-cache')

module.exports = async (user, updatedFields) => {
    userCache.clearCacheValue(user.id)
    userCache.clearCacheValue(user.name.toLowerCase())
    saveRowWithId('users', updatedFields, user.id)
}
