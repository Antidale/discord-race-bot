const createApiConnection = require('../util/create-api-connection')
const makeCallAndGetResponse = require('../util/make-call-and-get-response')

module.exports = async () => {
    const uri = createApiConnection('users/featured')
    const response = await makeCallAndGetResponse(uri, null, 'GET')
    return response
}
