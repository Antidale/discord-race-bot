const createApiConnection = require('../util/create-api-connection')
const makeCallAndGetResponse = require('../util/make-call-and-get-response')

module.exports = async (tableName, objWithId) => {
    const uri = createApiConnection(tableName)
    const response = await makeCallAndGetResponse(uri, objWithId, 'POST')
    return response
}
