const createApiConnection = require('./create-api-connection')
const makeCallAndGetResponse = require('./make-call-and-get-response')

module.exports = async (tableName, obj, id) => {
    const uri = createApiConnection(tableName, id)
    const response = await makeCallAndGetResponse(uri, obj, id ? 'PUT' : 'POST')
    return response
}
