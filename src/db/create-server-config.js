const saveRowWithId = require('./util/save-row-with-id')

module.exports = async guild => saveRowWithId('guilds', guild)
