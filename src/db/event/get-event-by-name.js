const createApiConnection = require('../util/create-api-connection')
const makeCallAndGetResponse = require('../util/make-call-and-get-response')

module.exports = async (name) => {
    if (!name) {
        return null
    }
    const uri = createApiConnection(`events?name=${name}`)
    const response = await makeCallAndGetResponse(uri, null, 'GET')
    return response
}
