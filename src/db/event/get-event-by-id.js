const getRowById = require('../util/get-row-by-id')

module.exports = async (id) => {
    const event = await getRowById('events', id)
    return event
}
