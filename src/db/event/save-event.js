const saveRowWithId = require('../util/save-row-with-id')

module.exports = async (event, updatedFields) => {
    saveRowWithId('events', updatedFields, event.id)
}
