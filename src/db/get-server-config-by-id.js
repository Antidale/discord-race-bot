const getRowById = require('./util/get-row-by-id')
const guildCache = require('../cache/guild-cache')

module.exports = async (id) => {
    let cachedValue = guildCache.getValue(id)
    if (!cachedValue) {
        cachedValue = await getRowById('guilds', id)
        guildCache.updateCacheValue(id, cachedValue)
    }
    return cachedValue
}
