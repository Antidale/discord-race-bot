const saveRowWithId = require('./util/save-row-with-id')
const raceCache = require('../cache/race-cache')

module.exports = async (race, updatedFields) => {
    raceCache.clearCacheValue(race.id)
    raceCache.clearCacheValue(race.key)
    saveRowWithId('races', updatedFields, race.id)
}
