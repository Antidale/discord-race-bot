const createApiConnection = require('../util/create-api-connection')
const makeCallAndGetResponse = require('../util/make-call-and-get-response')
const raceCache = require('../../cache/race-cache')

module.exports = async (key) => {
    if (!key) {
        return null
    }
    const cachedValue = raceCache.getValue(key)
    if (!cachedValue) {
        const uri = createApiConnection(`races?key=${key}`)
        const response = await makeCallAndGetResponse(uri, null, 'GET')
        if (response) {
            raceCache.updateCacheValue(key, response)
        }
        return response
    }
    return cachedValue
}
