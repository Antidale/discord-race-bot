const createApiConnection = require('../util/create-api-connection')
const makeCallAndGetResponse = require('../util/make-call-and-get-response')

module.exports = async (options) => {
    let optionParams = '?pageSize=1000'
    if (options) {
        if (options.completed) {
            optionParams += '&status=completed'
        }
        if (options.active) {
            optionParams += '&status=active'
        }
    }
    const uri = createApiConnection(`races${optionParams}`)
    const response = await makeCallAndGetResponse(uri, null, 'GET')
    return response
}
