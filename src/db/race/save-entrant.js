const saveRowWithId = require('../util/save-row-with-id')
const raceCache = require('../../cache/race-cache')

module.exports = async (race, entrant) => {
    raceCache.clearCacheValue(race.id)
    raceCache.clearCacheValue(race.key)
    saveRowWithId('races', entrant, `${race.id}/entrants/${entrant.id}`)
}
