const getRowById = require('../util/get-row-by-id')
const raceCache = require('../../cache/race-cache')

module.exports = async (id) => {
    const cachedValue = raceCache.getValue(id)
    if (!cachedValue) {
        const race = await getRowById('races', id)
        race.details.entrants = race.details.entrants.filter(en => en) || []
        race.details.finishers = race.details.finishers.filter(en => en) || []
        raceCache.updateCacheValue(id, race)
        return race
    }
    return cachedValue
}
