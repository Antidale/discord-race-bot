module.exports = {
    RUNNING: 'Running',
    WAITING: 'Waiting',
    READY: 'Ready',
    FORFEIT: 'Forfeited',
    FINISHED: 'Finished',
}
