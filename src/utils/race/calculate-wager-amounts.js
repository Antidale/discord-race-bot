const MAX_SPLIT_FOR_FIRST = 0.75
const MAX_SPLIT_FOR_SECOND = 0.75
const MAX_SPLIT_FOR_THIRD = 0.75

const COOKIES_FOR_FIRST = 3
const COOKIES_FOR_SECOND = 2
const COOKIES_FOR_FINISHING = 1

const minCookieMap = {
    1: COOKIES_FOR_FIRST,
    2: COOKIES_FOR_SECOND,
}
const winPercentageMap = {
    1: MAX_SPLIT_FOR_FIRST,
    2: MAX_SPLIT_FOR_SECOND,
    3: MAX_SPLIT_FOR_THIRD,
}
const donationMap = {
    1: 0.5,
    2: 0.35,
    3: 0.15,
}

module.exports = (race) => {
    const updatedRace = race
    const { totalWagered } = updatedRace.details
    const { donations } = updatedRace.details
    let totalDonated = 0
    if (donations) {
        totalDonated = donations.reduce((sum, donation) => sum + donation.amount, 0)
    }
    let amountToDistribute = totalWagered + updatedRace.details.finishers.length + (COOKIES_FOR_FIRST - 1) + (COOKIES_FOR_SECOND - 1)
    const numberOfWagers = race.details.entrants.filter(e => (e.wager || 0) > 0).length
    updatedRace.details.finishers.sort((a, b) => a.placement - b.placement).forEach((finisher) => {
        const updatedFinisher = finisher
        const allowedToWinFromDonation = Math.floor((updatedRace.details.finishers.length >= 3) ? totalDonated * (donationMap[finisher.placement] || 0) : totalDonated * (1.0 / updatedRace.details.finishers.length))
        const cookiesFromPlacement = minCookieMap[finisher.placement] || COOKIES_FOR_FINISHING
        const minCookies = (finisher.placement <= 3 ? (finisher.wager || 0) : 0) + cookiesFromPlacement
        let maxCookies = (finisher.wager || 0) + cookiesFromPlacement
        if (finisher.placement <= 3) {
            maxCookies += (finisher.wager || 0) * (numberOfWagers - 1)
        }
        const amountToAward = Math.floor(amountToDistribute * (winPercentageMap[finisher.placement] || 1))
        let actuallyAwarded = Math.min(Math.max(minCookies, amountToAward), maxCookies)
        if (actuallyAwarded > amountToDistribute) {
            if (actuallyAwarded > 1) {
                actuallyAwarded = (amountToDistribute || COOKIES_FOR_FINISHING)
            }
        }
        updatedFinisher.amountWon = actuallyAwarded + allowedToWinFromDonation
        amountToDistribute -= actuallyAwarded
    })
    return updatedRace
}
