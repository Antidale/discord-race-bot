const CONSTANTS = require('../../constants')

module.exports = (race) => {
    if (!race) { return false }
    if (!race.details) { return false }
    if (!race.details.status) { return false }
    return race.details.status === CONSTANTS.RACE_STATUS.LOCKED
}
