module.exports = async (race, channel) => {
    let message = `
\`\`\`
Basic Commands
--------------
!ready - (Indicate you are ready)
!unready - (Indicate you are no longer ready)
!quit - (Leave the race)
!forfeit - (Forfeit the race)
!done - (Finish the race)
!wager amount - (Wagers cookies on the race!)
!undone - (Indicate the !done was a mistake and go back to 'Running')
!setmetadata key value - (Sets the metadata for a race - can only be done via race creator or admin)
`
    // TODO: Refactor into not terrible
    if (race.key.startsWith('ff1r')) {
        message += `
Additional Commands
-------------------
!ff1flags flagstring - (Generates a FF1R seed given a flagstring)
`
    } else if (race.key.startsWith('ff4fe')) {
        message += `
Additional Commands
-------------------
!ff4flags flagstring - (Generates a FF4FE seed given a flagstring)
!ff4beta [--hidden] flagstring - (Generates a FF4FE seed given a flagstring - using the beta site)
`
    } else if (race.key.startsWith('smrpgr')) {
        message += `
Additional Commands
-------------------
!smrpgflags flagstring - (Generates a SMRPGR seed given a flagstring)
`
    } else if (race.key.startsWith('hades')) {
        message += `
Additional Commands
-------------------
!weapon [--nospoiler] - Choose a random weapon. If --nospoiler is included, hidden aspects will be excluded.
!hadesheat [--minheat x] [--maxheat y] - Chooses a heat between minHeat and maxHeat (default Min=0, Max=32)
`
    }
    message += '\n```'
    channel.send(message)
    if (race.details.async) {
        channel.send('Don\'t forget to `!startasync` to finalize the race details!\nDon\'t forget to `!finishasync` when the race is over!')
    }
    if (race.details.official) {
        channel.send('This is an **official** race room.  Only admins may modify the race details, such as seed.')
    }
}
