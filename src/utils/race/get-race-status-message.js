module.exports = (race) => {
    if (!race) {
        return 'No race was found, have a developer check for errors.'
    }
    let raceMessage = `${race.key} (${race.details.mode}) - Status: ${race.details.status} | ${race.details.entrants.length} Entrants`
    if (race.details.metadata) {
        if (race.details.metadata.Hash) {
            raceMessage += ` | ${race.details.metadata.Hash}`
        }
        if (race.details.metadata.Url) {
            raceMessage += ` | ${race.details.metadata.Url}`
        }
        if (race.details.metadata.Flags) {
            raceMessage += ` | ${race.details.metadata.Flags}`
        }
        if (race.details.metadata.Title) {
            raceMessage = `${race.details.metadata.Title} - ${raceMessage}`
        }
    }
    if (race.event) {
        raceMessage = `Event: ${race.event.name} - ${raceMessage}`
    }
    return raceMessage
}
