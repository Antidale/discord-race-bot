const logger = require('./logger')

module.exports = async (guild, channelName, channelType, channelCategory, adminRole, botRole) => {
    if (!guild) { return null }
    let parent = null
    if (channelCategory) {
        const cat = guild.channels.cache.find(ch => ch.name === channelCategory)
        if (cat) {
            parent = cat
        }
    }
    const options = {
        type: channelType,
        name: channelName,
        parent,
        permissionOverwrites: [
            {
                id: guild.roles.cache.find(role => role.name === '@everyone'),
                deny: ['SEND_MESSAGES', 'VIEW_CHANNEL'],
            },
            {
                id: guild.roles.cache.find(role => role.name === botRole),
                allow: ['VIEW_CHANNEL', 'MANAGE_CHANNELS', 'MOVE_MEMBERS', 'SEND_MESSAGES', 'MANAGE_MESSAGES'],
            },
            {
                id: guild.roles.cache.find(role => role.name === adminRole),
                allow: ['VIEW_CHANNEL', 'MANAGE_CHANNELS', 'SEND_MESSAGES', 'MANAGE_MESSAGES'],
            },
        ],
    }
    const channel = await guild.channels.create(channelName, options)
    logger.logDebug(`Channel ${channel.name} created successfully`)
    return channel
}
