/* eslint-disable */
module.exports = (message) => {
  if (!message.content) { return null }
  // Magical function stolen from the below
  // https://stackoverflow.com/questions/2817646/javascript-split-string-on-space-or-on-quotes-to-array

  let parsed = message.content.match(/\\?.|^$/g).reduce((p, c) => {
    if (c === '"') {
      p.quote ^= 1
    } else if (!p.quote && c === ' ') {
      p.a.push('')
    } else {
      p.a[p.a.length - 1] += c.replace(/\\(.)/, '$1')
    }
    return p
  }, { a: [''] }).a
  parsed = parsed.filter(arg => arg !== '')
  return parsed
}
