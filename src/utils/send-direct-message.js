
const getDMChannel = require('./get-dm-channel')
const sendMessage = require('./send-message')

module.exports = async (user, message) => {
    if (user.bot) { return } // Do not attempt to send messages to bot users
    const channel = await getDMChannel(user)
    if (message) {
        await sendMessage(channel, message)
    }
}
