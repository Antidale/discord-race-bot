const encodePassword = require('./encode-password')

module.exports = (encodedPassword, password) => {
    const encoded = encodePassword(password)
    return (encoded === encodedPassword)
}
