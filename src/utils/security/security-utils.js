const isMessageOnRaceServer = (message, race) => {
    if (!message || !race) { return false }
    if (!message.guild || !race.details.guild) { return false }
    if (!message.guild.id || !race.details.guild.id) { return false }
    return message.guild.id === race.details.guild.id
}
const isSentByCreatorOrAdmin = (message, race, hasAdmin) => {
    if (hasAdmin) { return true }
    if (!message || !race) {
        return false
    }
    return message.author.id === race.details.creator.id
}

const verifyRaceMessage = (message, race) => {
    if (!isMessageOnRaceServer(message, race)) {
        return `This message must be sent on ${race.details.guild.name}`
    }
    return null
}

module.exports = {
    isMessageOnRaceServer,
    isSentByCreatorOrAdmin,
    verifyRaceMessage,
}
