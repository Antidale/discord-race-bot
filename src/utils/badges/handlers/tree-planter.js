const THRESHOLD = 100
module.exports = (message, race, user, badge) => {
    if (!user) { return null }
    if (user.race_details.seeds_rolled >= THRESHOLD) {
        const award = Object.assign({ earnedFrom: race.key }, badge)
        return award
    }
    return null
}
