// const gambler = require('./gambler')
// const grandmaster = require('./grandmaster')
const highroller = require('./high-roller')
const raceEnthusiast = require('./race-enthusiast')
const jackpot = require('./jackpot')
const treePlanter = require('./tree-planter')

module.exports = {
    // gambler,
    // grandmaster,
    'high-roller': highroller,
    'race-enthusiast': raceEnthusiast,
    jackpot,
    'tree-planter': treePlanter,
}
