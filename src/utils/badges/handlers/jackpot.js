const THRESHOLD = 10
const MIN_BET = 10

module.exports = (message, race, user, badge) => {
    if (!race || !user) { return null }
    const entrant = race.details.finishers.find(en => en.id === user.id)
    if (entrant && entrant.wager >= MIN_BET && entrant.amountWon >= (entrant.wager * THRESHOLD)) {
        const award = Object.assign({ earnedFrom: race.key }, badge)
        return award
    }
    return null
}
