const THRESHOLD = 100
module.exports = (message, race, user, badge) => {
    if (!race || !user) { return null }
    const entrant = race.details.entrants.find(en => en.id === user.id)
    if (!entrant || !entrant.wager) { return null }
    if (entrant.wager >= THRESHOLD) {
        const award = Object.assign({ earnedFrom: race.key }, badge)
        return award
    }
    return null
}
