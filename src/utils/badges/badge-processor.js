const { badges } = require('../../../data/badges.json')
const badgeHandlers = require('./handlers')
const userHasBadge = require('./user-has-badge')

module.exports = (message, race, user) => {
    const availableBadges = badges.filter(badge => !userHasBadge(user, badge))
    const badgesToHandle = availableBadges.filter(badge => badge.handler)
    const newBadgesEarned = badgesToHandle.map(badge => badgeHandlers[badge.handler](message, race, user, badge)).filter(result => result)
    return newBadgesEarned
}
