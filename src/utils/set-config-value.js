const logger = require('./logger')
const getServerConfig = require('../db/get-server-config-by-id')
const saveConfig = require('../db/save-server-config')
const createConfig = require('../db/create-server-config')

module.exports = async (message, args, hasPermissions) => {
    if (!message.guild) {
        return null
    }
    if (!args || args.length < 2) {
        return null
    }
    if (!hasPermissions) {
        return null
    }
    const guildId = message.guild.id
    if (!guildId) {
        return null
    }
    let config = await getServerConfig(guildId)
    const [configKey, configValue] = args
    if (!config) {
        logger.logInfo(`No config found for ${guildId} - creating`)
        config = { id: guildId }
        await createConfig(config)
    }
    if (configValue !== undefined && configValue !== null) {
        config[configKey] = configValue
        const updated = { }
        updated[configKey] = configValue
        saveConfig(config, updated, guildId)
    }
    return config
}
