const moment = require('moment')
const CONSTANTS = require('../../constants')

const MILLIS_PER_MINUTE = 60 * 1000
const MINUTES_ADD_FOR_FORFEIT = 5
const calculateTime = (finisher, defaultTime) => {
    if (finisher.finish && finisher.finish !== CONSTANTS.ENTRANT_STATUS.FORFEIT) {
        return moment.duration(finisher.finish).asMilliseconds()
    }
    return defaultTime
}
const getFinisherInfo = (finisher, defVal) => ({ name: finisher.name, id: finisher.id, time: calculateTime(finisher, defVal) })
const getAverage = ary => ary.reduce((total, value) => total + value, 0) / ary.length
module.exports = (finishers, minutesToAddPerFF = MINUTES_ADD_FOR_FORFEIT) => {
    // Drop all forfeits before calculating    

    if (finishers.length < 4) {
        return finishers.map(finisher => ({
            name: finisher.name,
            id: finisher.id,
            score: 'N/A',
        }))
    }
    const finishTimes = finishers.map(finisher => getFinisherInfo(finisher, 0))
    const maxFinish = Math.max(...finishTimes.map(ft => moment.duration(ft.time).asMilliseconds()))
    const forfeitTime = maxFinish + (minutesToAddPerFF * MILLIS_PER_MINUTE)
    const actualFinishers = finishers.filter(finisher => finisher.finish && finisher.finish !== CONSTANTS.ENTRANT_STATUS.FORFEIT).map(finisher => getFinisherInfo(finisher, 0))
    const sortedTimes = actualFinishers.sort((a, b) => Number(a.time) - Number(b.time))
    const avgTime = getAverage(sortedTimes.map(t => t.time))
    const squareDiffs = sortedTimes.map((value) => {
        const diff = value.time - avgTime
        return diff * diff
    })
    const avgSquares = getAverage(squareDiffs)
    const stdDev = Math.sqrt(avgSquares)
    return finishers.map(finisher => ({
        name: finisher.name,
        id: finisher.id,
        score: (calculateTime(finisher, forfeitTime) - avgTime) / stdDev,
    }))
}
