const moment = require('moment')

module.exports = (dataset, filter) => {
    if (!dataset || dataset.length === 0) return dataset
    let theDataset = dataset
    switch (filter.name) {
    case '--game':
        theDataset = theDataset.filter(race => race.details.type.some(t => t.includes(filter.value)))
        break
    case '--start':
        theDataset = theDataset.filter(race => moment(race.details.startTime).diff(moment(filter.value)) >= 0)
        break
    case '--end':
        theDataset = theDataset.filter(race => moment(race.details.finishTime).diff(moment(filter.value)) <= 0)
        break
    case '--numentrants':
        theDataset = theDataset.filter(race => race.details.entrants.length >= Number(filter.value))
        break
    case '--ff4flags':
        theDataset = theDataset.filter(row => row.details.metadata && row.details.metadata.Flags === filter.value)
        break
    case '--entrant':
        theDataset = theDataset.filter(row => row.details.entrants.some(entrant => entrant.name && entrant.name.toLowerCase() === filter.value.toLowerCase()))
        break
    case '--team':
        theDataset = theDataset.filter(row => row.details.entrants.some(entrant => entrant.team && entrant.team.toLowerCase() === filter.value.toLowerCase()))
        break
    case '--roller':
        theDataset = theDataset.filter(row => row.details.metadata && row.details.metadata.Roller && row.details.metadata.Roller.toLowerCase() === filter.value.toLowerCase())
        break
    default:
        break
    }
    return theDataset
}
