module.exports = () => ({
    race_details: {
        races_run: 0,
        races_first: 0,
        races_second: 0,
        races_third: 0,
        races_forfeit: 0,
        races_completed: [],
        personal_best: null,
        personal_best_race: null,
        seeds_rolled: 0,
    },
})
