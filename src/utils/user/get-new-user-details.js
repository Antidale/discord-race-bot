const getNewUserStats = require('./get-new-user-stats')

module.exports = (user) => {
    let userDetails = {
        name: user.username,
        number: user.discriminator,
        cookies: 5,
        id: user.id,
        guildDetails: { },
    }
    userDetails = Object.assign(userDetails, getNewUserStats())
    return userDetails
}
