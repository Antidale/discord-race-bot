const getRaces = require('../../db/race/get-races')
const CONSTANTS = require('../../constants')

const ACTIVE_STATUSES = [CONSTANTS.ENTRANT_STATUS.READY, CONSTANTS.ENTRANT_STATUS.RUNNING, CONSTANTS.ENTRANT_STATUS.WAITING]
const isActiveUser = (entrant, user) => entrant && entrant.id === user.id && ACTIVE_STATUSES.indexOf(entrant.status) >= 0

module.exports = async (user) => {
    const activeRaces = await getRaces({ active: true })
    if (!activeRaces) { return null }
    const activeRacesWithUser = activeRaces.filter(race => race.details.entrants.some(entrant => isActiveUser(entrant, user)) && !race.details.async)
    if (activeRacesWithUser.length < 0) {
        return null
    }
    return activeRacesWithUser[0]
}
