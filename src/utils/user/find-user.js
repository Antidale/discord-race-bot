
const getUserByName = require('../../db/user/get-user-by-name')

module.exports = async (name, guild) => {
    if (!name) { return null }
    if (typeof guild === 'object' && guild && guild.id) {
        return getUserByName(name, guild.id)
    }
    // First try to find exact match
    return getUserByName(name, guild)
}
