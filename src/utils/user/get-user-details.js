const getNewUserDetails = require('./get-new-user-details')
const saveUserData = require('../../db/user/save-user')
const createUserData = require('../../db/user/create-user')
const getUserById = require('../../db/user/get-user-by-id')
// const updateUserNickname = require('./update-user-nickname')

module.exports = async (message) => {
    if (!message || !message.author) { return null }
    const userId = message.author.id
    // const guildId = message.guild.id
    let userDetails = await getUserById(userId)
    if (!userDetails) {
        userDetails = getNewUserDetails(message.author)
        await createUserData(userDetails)
    }
    const updatedDetails = {}
    if (userDetails.number !== message.author.discriminator) {
        userDetails.number = message.author.discriminator
        updatedDetails.number = userDetails.number
    }
    if (userDetails.name !== message.author.username) {
        userDetails.name = message.author.username
        updatedDetails.name = message.author.username
    }
    if (Object.keys(updatedDetails).length > 0) {
        await saveUserData(userDetails, updatedDetails)
    }
    return userDetails
}
