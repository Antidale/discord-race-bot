const logger = require('../../utils/logger')

module.exports = async (userNameAndDiscriminator, client) => {
    // First check for a match in the cached users, and if no matches then look in each guild
    let foundUser = client.users.cache.find((user) => {
        if (!user.tag) {
            logger.warn('User tag in client list is undefined or empty', user)
            return false
        }
        return user.tag.toLowerCase() === userNameAndDiscriminator.toLowerCase()
    })
    if (foundUser) { return foundUser }
    const allUsers = client.guilds.cache.map(guild => guild.users.cache).reduce((arr, item) => arr.concat(item), []).filter(i => i)
    foundUser = allUsers.find((user) => {
        if (!user.tag) {
            logger.warn('User tag in guild list is undefined or empty', user)
            return false
        }
        return user.tag.toLowerCase() === userNameAndDiscriminator.toLowerCase()
    })
    if (!foundUser) {
        const [un, disc] = userNameAndDiscriminator.split('#')
        // Attempt to find without using tag
        foundUser = allUsers.find(user => user.username.toLowerCase() === un && user.discriminator.toLowerCase() === disc.toLowerCase())
        if (!foundUser) {
            logger.warn(`No users found for ${userNameAndDiscriminator}`)
            const potentialUsers = allUsers.filter(user => user.username.toLowerCase() === un || user.discriminator.toLowerCase() === disc)
            logger.warn('Potential Users:', potentialUsers.map(user => `${user.tag} - ${user.username} - ${user.discriminator} - ${user.id}`.join(' | ')))
        }
    }
    return foundUser
}
