
// Entity can be a role or a user
module.exports = async (channel, entity, permissionSet) => {
    if (!permissionSet || !channel || !entity) {
        return null
    }
    if (channel.permissionOverwrites.find(po => po.id === entity)) {
        return channel.updateOverwrite(entity, permissionSet)
    }
    return channel.createOverwrite(entity, permissionSet)
}
