module.exports = async (user, guild) => {
    if (user.dmChannel) {
        return user.dmChannel
    }
    if (user.createDM) {
        return user.createDM()
    }
    // A discord user object wasn't passed in, try to look up the user from the id
    if (!user.id) { return null }
    if (!guild) { return null }
    const guildMember = await guild.members.fetch(user.id)
    if (!guildMember) { return null }
    if (guildMember.user.dmChannel) {
        return guildMember.user.dmChannel
    }
    if (guildMember.user.createDM) {
        return guildMember.user.createDM()
    }
    return null
}
