const utils = require('../race/race-utils')
const logger = require('../../utils/logger')
const saveRace = require('../../db/save-race')
const CONSTANTS = require('../../constants/index')
const getConfigValue = require('../../utils/get-config-value')
const securityUtils = require('../../utils/security/security-utils')
const isRaceStartable = require('../../utils/race/is-race-startable')

const getChannelName = (race, team) => `${race.key}-${team.replace(/ /, '-')}`

module.exports = async (message, args, isAdmin) => {
    const race = await utils.getCurrentRace(message.channel.name)
    if (!race) { return null }
    const securityResult = securityUtils.verifyRaceMessage(message, race)
    if (securityResult) {
        return securityResult
    }
    if (!isRaceStartable(race)) {
        return `Cannot create voice channels for a race in the ${race.details.status} status.`
    }
    const teams = new Set(race.details.entrants.map(entrant => entrant.team))
    const raceCategory = await getConfigValue(message, CONSTANTS.SERVER_CONFIG.CATEGORY_KEY, CONSTANTS.CONFIG.DEFAULT_RACE_CATEGORY)
    const maxAllowedChannels = await getConfigValue(message, CONSTANTS.SERVER_CONFIG.MAX_VOICE_ADMIN, CONSTANTS.CONFIG.DEFAULT_MAX_VOICE)
    if (teams.size >= maxAllowedChannels) {
        if (!isAdmin) {
            return 'The maximum number of voice channels that can be created by a non-admin has been reached.  Please ask an admin to create the voice channels.'
        }
    }
    const adminRole = await getConfigValue(message, CONSTANTS.SERVER_CONFIG.BOT_ADMIN_ROLE, CONSTANTS.CONFIG.DEFAULT_ADMIN_ROLE)
    const activeVoiceChannels = message.guild.channels.cache.filter(channel => channel.type === 'voice' && channel.parent && channel.parent.name === raceCategory)
    logger.logDebug(`There are ${activeVoiceChannels.size} active voice channels`)
    if (activeVoiceChannels.size >= maxAllowedChannels) {
        if (!isAdmin) {
            return 'The maximum number of voice channels that can be created by a non-admin has been reached.  Please ask an admin to create the voice channels.'
        }
    }
    const channelList = [...teams].map(team => getChannelName(race, team))
    teams.forEach((team) => {
        const channelName = getChannelName(race, team)
        const existingChannel = message.guild.channels.cache.find(channel => channel.name === channelName && channel.type === 'voice')
        const teamMembers = race.details.entrants.filter(entrant => entrant.team === team)
        if (!existingChannel) {
            message.guild.channels.create(channelName, { type: 'voice' })
                .then((channel) => {
                    logger.logDebug(`Channel ${channel.name} created successfully`)
                    channel.createOverwrite(message.guild.roles.cache.find(role => role.name === '@everyone'), { VIEW_CHANNEL: false })
                    channel.createOverwrite(message.guild.roles.cache.find(role => role.name === adminRole), {
                        VIEW_CHANNEL: true, CONNECT: true, MANAGE_CHANNELS: true, MOVE_MEMBERS: true,
                    })
                    teamMembers.forEach((teamMember) => {
                        channel.createOverwrite(teamMember.id, {
                            VIEW_CHANNEL: true, SPEAK: true, CONNECT: true, USE_VAD: true,
                        })
                    })
                    if (raceCategory) {
                        const cat = message.guild.channels.cache.find(ch => ch.name === raceCategory)
                        if (cat) {
                            channel.setParent(cat)
                        }
                    }
                    saveRace(race)
                })
                .catch(logger.logError)
        } else {
            teamMembers.forEach((teamMember) => {
                existingChannel.createOverwrite(teamMember.id, {
                    VIEW_CHANNEL: true, SPEAK: true, CONNECT: true, USE_VAD: true,
                })
            })
        }
    })
    // Delete all unused voice channels
    const unusedVoiceChannels = message.guild.channels.cache.filter(channel => channel.name.includes(race.key) && channel.type === 'voice' && channelList.indexOf(channel.name) < 0)
    if (unusedVoiceChannels && unusedVoiceChannels.size > 0) {
        unusedVoiceChannels.forEach(ch => ch.delete())
    }
    return null
}
