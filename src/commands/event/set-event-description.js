const getEventByName = require('../../db/event/get-event-by-name')
const saveEvent = require('../../db/event/save-event')
const hasEventPermissions = require('../../utils/event/has-event-permissions')

module.exports = async (message, args) => {
    if (!args || args.length === 0) { return null }
    const eventName = args.includes('--name') ? args[args.indexOf('--name') + 1] : args[0]
    const newDescription = args.includes('--description') ? args[args.indexOf('--description') + 1] : args[1]
    if (!newDescription) {
        return 'You must provide a description!'
    }
    const existingEvent = await getEventByName(eventName)
    if (!existingEvent) {
        return `No event with name ${eventName} exists.`
    }
    if (!hasEventPermissions(existingEvent, message.author, 'Lead')) {
        return 'You do not have the necessary permissions to change the event description.'
    }
    const saveDetails = { description: newDescription }
    await saveEvent(existingEvent, saveDetails)
    return `Description for ${eventName} modified successfully`
}
