const moment = require('moment')
const getEventByName = require('../../db/event/get-event-by-name')
const saveEvent = require('../../db/event/save-event')
const hasEventPermissions = require('../../utils/event/has-event-permissions')
const getEventById = require('../../db/event/get-event-by-id')
const sendDirectMessage = require('../../utils/send-direct-message')
const getEventDetails = require('../../utils/event/get-event-details')

const buildHelpMessage = () => `
Usage:  !endevent event-name
    Example: !endevent "my awesome event"
    event-name: The name of your event. Event names are unique.
`

module.exports = async (message, args) => {
    if (!args || args.length === 0 || args.includes('--help')) {
        return buildHelpMessage()
    }
    const eventName = args.includes('--name') ? args[args.indexOf('--name') + 1] : args[0]
    const existingEvent = await getEventByName(eventName)
    if (!existingEvent) {
        return `Event ${eventName} not found.`
    }
    if (!hasEventPermissions(existingEvent, message.author, 'Lead')) {
        return 'You do not have the necessary permissions to close this event.'
    }
    existingEvent.status = 'Completed'
    existingEvent.finishTime = moment().toISOString(true)
    await saveEvent(existingEvent, { status: existingEvent.status, finishTime: existingEvent.finishTime })
    const updatedEvent = await getEventById(existingEvent.id)
    const eventResults = getEventDetails(updatedEvent)
    updatedEvent.entrants.forEach((entrant) => {
        sendDirectMessage({ id: entrant.id }, eventResults)
    })
    return `Event ${eventName} has been completed.`
}
