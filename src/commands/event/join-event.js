const getEventByName = require('../../db/event/get-event-by-name')
const checkPassword = require('../../utils/event/check-join-password')
const addEventEntrant = require('../../db/event/add-event-entrant')

const buildHelpMessage = () => `
Usage:  !joinevent event-name [--password password] [--team team-name]
    Note: When using --team please make sure your teammate(s) all use the same spelling, capitalization, and punctuation.
`

module.exports = async (message, args) => {
    if (!args || args.length === 0 || args.includes('--help')) {
        return buildHelpMessage()
    }
    const eventName = args.includes('--name') ? args[args.indexOf('--name') + 1] : args[0]
    const existingEvent = await getEventByName(eventName)
    if (!existingEvent) {
        return `Event ${eventName} not found.`
    }
    if (existingEvent.visibility === 'private') {
        if (message.guild) {
            message.delete()
            return 'You must DM the bot in order to join a private event.'
        }
        if (!args.includes('--password')) {
            return `${eventName} is a private event and requires a password to join.`
        }
        const eventPassword = args[args.indexOf('--password') + 1]
        if (!checkPassword(existingEvent, eventPassword)) {
            return 'Invalid password.'
        }
    }
    const entrant = message.author
    if (existingEvent.entrants.find(e => e.id === entrant.id)) {
        return `You are already a participant in ${eventName}`
    }
    const team = args.includes('--team') && existingEvent.entrantType === 'TEAM' ? args[args.indexOf('--team') + 1] : entrant.username
    if (existingEvent.entrantType === 'TEAM' && team === entrant.username) {
        return 'This is a team event. You must sign up with a team name using --team.  Please make sure you and your teammate(s) use the same capitalization, spelling, and punctuation.' // eslint-disable-line max-len
    }
    await addEventEntrant(existingEvent, {
        id: entrant.id,
        name: entrant.username,
        full: entrant.tag,
        matches: [],
        races: [],
        team,
        placement: 0,
        points: 0,
        seed: 0,
    })
    return `You have joined ${eventName}.`
}
