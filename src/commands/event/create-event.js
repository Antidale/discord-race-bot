const getEventByName = require('../../db/event/get-event-by-name')
const saveEvent = require('../../db/event/save-event')

const validEventTypes = ['LEAGUE', 'SINGULAR']

const buildHelpMessage = () => `\`\`\`
Usage:  !createevent event-name --type event-type [--server server-id] [--entrant-type entrant-type]
Example: !createevent "The Amazing Race" --type LEAGUE
    event-name: The name of your event. Event names are unique.
    event-type: Can be one of [LEAGUE, TOURNAMENT, SINGULAR]
        LEAGUE: A recurring event where all players continue to play and receive points for each match played
        TOURNAMENT*: (Coming Soon :tm:) A recurring event where players are eliminated until only one remains
        SINGULAR: A non-recurring event
    entrant-type: Can be one of [SOLO, TEAM]
        SOLO: Individuals earn points
        TEAM: Teams earn points
    server (optional): If the league needs to be restricted to a particular discord server, enter its ID here (If you do not know this, ask the RaceBot Owner for help)
\`\`\``

module.exports = async (message, args) => {
    if (!args || args.length === 0 || args.includes('--help')) {
        return buildHelpMessage()
    }
    if (!args.includes('--type')) {
        return '!createevent command is missing the required parameters.  `!createevent --help` for more information'
    }
    const eventName = args.includes('--name') ? args[args.indexOf('--name') + 1] : args[0]
    const eventType = args[args.indexOf('--type') + 1]
    if (!validEventTypes.includes(eventType)) {
        return `Event type must be one of [${validEventTypes.join(',')}]`
    }
    let server = ''
    if (args.includes('--server')) {
        server = args[args.indexOf('--server') + 1]
    }
    const existingEvent = await getEventByName(eventName)
    if (existingEvent) {
        return `Event ${eventName} already exists. Please choose a new name for your event.`
    }
    let entrantType = ''
    if (args.includes('--entrant-type')) {
        entrantType = args[args.indexOf('--entrant-type') + 1]
    }
    const newEvent = {
        name: eventName,
        type: eventType,
        status: 'Running',
        entrants: [],
        races: [],
        teams: [],
        matches: [],
        admins: [{
            id: message.author.id,
            full: message.author.tag,
            name: message.author.username,
            role: 'Creator',
        }],
        entrantType: entrantType || 'SOLO',
        server: server || 'ALL',
    }
    await saveEvent(newEvent, newEvent)
    return `${eventName} (${eventType}) created successfully.`
}
