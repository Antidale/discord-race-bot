const getUserDetails = require('../../utils/user/get-user-details')
const updateCookies = require('../../db/user/update-cookies')
const rewards = require('../../../data/rewards.json')
const addReward = require('./add-reward')

module.exports = async (message, args) => {
    if (!args || args.length === 0) {
        return 'You must choose a reward to buy!'
    }
    const [reward] = rewards.rewards.filter(re => re.name.toLowerCase().replace(' ', '') === args[0].toLowerCase().replace(' ', ''))
    if (!reward) {
        return `${args[0]} not found as a reward. Please use \`!rewards\` to view all available rewards!`
    }
    const userInfo = await getUserDetails(message)
    const { cost, name, type } = reward
    if (userInfo.cookies < cost) {
        return `${name} costs ${cost} cookies and you only have ${userInfo.cookies}...`
    }
    if (type === 'Badge') {
        const existingBadge = (userInfo.badges || []).some(b => b && b.name && b.name.toLowerCase() === name.toLowerCase())
        if (existingBadge) {
            return `You already own the ${name} badge!`
        }
    }
    updateCookies(userInfo, -cost, '!buyreward', `Bought ${name} for ${cost} cookies.`)
    addReward(userInfo, reward)
    return `${userInfo.name} bought ${name} for ${cost}!` // eslint-disable-line
}
