const { badges } = require('../../../data/badges.json')
const buildTable = require('../../utils/table-layout')

module.exports = async () => {
    const headers = ['Name', 'Description']
    const data = badges.map(badge => ({ name: badge.name, desc: badge.description }))
    return `\`\`\`\n${buildTable(headers, data)}\n\`\`\``
}
