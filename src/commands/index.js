const adminCommands = require('./admin')
const raceCommands = require('./race')
const userCommands = require('./user')
const voiceCommands = require('./voice')
const statCommands = require('./stats')
const gameCommands = require('./game')
const rewardCommands = require('./rewards')
const eventCommands = require('./event')
const logger = require('../utils/logger')
const hasBotAdmin = require('../utils/has-bot-admin')
const sendMessage = require('../utils/send-message')
const parseMessageArgs = require('../utils/parse-message-args')
const help = require('./help/help-command')
const getGuildDetails = require('../db/get-server-config-by-id')
const createGuildDetails = require('../db/create-server-config')

module.exports = (client) => {
    const commandList = Object.assign({}, ...[adminCommands, raceCommands, userCommands, voiceCommands, gameCommands, statCommands, rewardCommands, eventCommands])

    const checkForBannedGuild = async (guild) => {
        const guildDetails = await getGuildDetails(guild.id)
        if (!guildDetails) {
            const config = { id: guild.id }
            createGuildDetails(config)
            return
        }
        if (guildDetails.banned) {
            logger.logInfo(`${guild.id} (${guild.name}) is on the banned list.  Leaving...`)
            guild.leave()
        }
    }

    client.on('message', async (message) => {
        // Make sure the bot never tries to respond to itself or another bot
        if (message.author.id === client.user.id || message.author.bot) { return }
        const commandArgs = parseMessageArgs(message)
        if (!commandArgs || commandArgs.length === 0) { return }
        const [commandName, ...args] = commandArgs
        const command = commandList[commandName]
        if (command) {
            logger.logInfo(`${message.author.username}(${message.author.id}) - ${message.content}`)
            const response = await command(message, args, await hasBotAdmin(message))
            if (response) {
                sendMessage(message.channel, response)
            }
        }
    })

    client.on('guildCreate', async (guild) => {
        logger.logInfo(`Joined Guild: ${guild.id} (${guild.name})`)
        checkForBannedGuild(guild)
    })

    client.on('ready', () => {
        logger.logInfo(`Connected as ${client.user.tag}`)
        client.guilds.cache.forEach((guild) => {
            logger.logInfo(` - ${guild.name} - ${guild.id}`)
            checkForBannedGuild(guild)
        })
    })

    client.on('error', logger.logError)

    // TODO: eventually build the help directly into each command as a property
    help.init(client)
}
