const randomstring = require('randomstring')
const fetch = require('node-fetch')
const utils = require('../../race/race-utils')
const securityUtils = require('../../../utils/security/security-utils')
const saveRace = require('../../../db/save-race')
const isRaceEditable = require('../../../utils/race/is-race-editable')

const baseUrl = 'http://ff6wc.com/flags'
const SEED_LENGTH = 16

const setFlags = (flagString) => {
    const seed = randomstring.generate({ length: SEED_LENGTH, charset: 'alphanumeric', capitalization: 'uppercase' })
    return {
        seed,
        flags: flagString,
    }
}
const updateRaceDetails = async (message, seedDetails, hasAdmin) => {
    if (!message || !message.channel) { return }
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace) { return }
    if (currentRace.details.official && !hasAdmin) {
        message.channel.send('Only admins can update the race seed.  Please do not use this seed for the race.')
        return
    }
    if (!isRaceEditable(currentRace)) {
        message.channel.send('The flags cannot be updated once the race has been started.  Please do not use this seed for the race.')
        return
    }
    if (!securityUtils.isMessageOnRaceServer(message, currentRace)) {
        message.channel.send(`The flags can only be updated on ${currentRace.details.guild.name}. Please do not use this seed for the race.`)
        return
    }
    const metadata = {
        Seed: seedDetails.seed,
        Flags: seedDetails.flags,
        Url: seedDetails.url,
        Roller: message.author.username,
    }
    if (currentRace.details.metadata) {
        currentRace.details.metadata = Object.assign(currentRace.details.metadata, metadata)
    } else {
        currentRace.details.metadata = metadata
    }
    message.channel.setTopic(`${seedDetails.url}`)
    saveRace(currentRace, { details: { metadata: currentRace.details.metadata } })
}
module.exports = async (message, args, hasAdmin) => {
    if (!args || args.length === 0) {
        return 'No flags were given.'
    }
    const fullFlagset = args.join(' ')
    const flagInfo = setFlags(fullFlagset)
    const generationUrl = `${baseUrl}/${encodeURI(`${flagInfo.flags} -s ${flagInfo.seed}`)}`
    const response = await fetch(generationUrl)
    const json = await response.json()
    const shareUrl = json.share_url
    await updateRaceDetails(message, { seed: flagInfo.seed, flags: flagInfo.flags, url: shareUrl }, hasAdmin)
    return `Your seed can be accessed at ${shareUrl}`
}
