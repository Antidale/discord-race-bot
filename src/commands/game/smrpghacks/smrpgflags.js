const fetch = require('node-fetch')
const randomWords = require('random-words')
const sendMessage = require('../../../utils/send-message')
const utils = require('../../race/race-utils')
const saveRace = require('../../../db/save-race')
const securityUtils = require('../../../utils/security/security-utils')
const isRaceEditable = require('../../../utils/race/is-race-editable')

const baseUrl = 'https://randomizer.smrpgspeedruns.com'
const apiUrl = 'https://randomizer.smrpgspeedruns.com/api/v1/generate'

const SEED_LENGTH = 10
const setFlags = (flagString) => {
    const seed = randomWords(SEED_LENGTH).join(' ').toUpperCase()
    return {
        seed,
        flags: flagString,
    }
}
const updateRaceDetails = async (message, seedDetails, hasAdmin) => {
    if (!message || !message.channel) { return }
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace) { return }
    if (currentRace.details.official && !hasAdmin) {
        message.channel.send('Only admins can update the race seed.  Please do not use this seed for the race.')
        return
    }
    if (!isRaceEditable(currentRace)) {
        message.channel.send('The flags cannot be updated once the race has been started.  Please do not use this seed for the race.')
        return
    }
    if (!securityUtils.isMessageOnRaceServer(message, currentRace)) {
        message.channel.send(`The flags can only be updated on ${currentRace.details.guild.name}. Please do not use this seed for the race.`)
        return
    }
    const retrieveUrl = `${baseUrl}${seedDetails.permalink}`
    const metadata = {
        Version: seedDetails.logic,
        Seed: seedDetails.seed,
        Flags: seedDetails.flag_string,
        Url: retrieveUrl,
        Character: seedDetails.file_select_character,
        Verification: seedDetails.file_select_hash,
        Roller: message.author.username,
    }
    if (currentRace.details.metadata) {
        currentRace.details.metadata = Object.assign(currentRace.details.metadata, metadata)
    } else {
        currentRace.details.metadata = metadata
    }
    message.channel.setTopic(`${retrieveUrl} : ${seedDetails.file_select_hash}`)
    saveRace(currentRace, { details: { metadata: currentRace.details.metadata } })
}
module.exports = async (message, args, hasAdmin) => {
    if (!args || args.length === 0) {
        return 'No flags were given.'
    }
    const fullFlagset = args.join(' ')
    const flagInfo = setFlags(fullFlagset)
    fetch(apiUrl, {
        method: 'post',
        body: JSON.stringify({
            flags: flagInfo.flags,
            seed: flagInfo.seed,
        }),
        headers: {
            'Content-Type': 'application/json',
        },
    })
        .then(res => res.json())
        .then((json) => {
            updateRaceDetails(message, json, hasAdmin)
            sendMessage(message.channel, `The seed can be accessed at <${baseUrl}${json.permalink}>`)
        })
        .catch((error) => {
            if (error) {
                sendMessage(message.channel, `Error generating seed: ${error}`)
            }
        })
    return 'Seed generation pending...'
}
