const chooseWeapon = require('./choose-weapon')
const chooseHeat = require('./choose-heat')

module.exports = {
    '!weapon': chooseWeapon,
    '!hadesheat': chooseHeat,
}
