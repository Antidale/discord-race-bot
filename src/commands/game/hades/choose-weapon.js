const WEAPONS = [
    {
        name: 'Stygius (Sygian Blade)',
        aspects: [
            {
                name: 'Zagreus',
            },
            {
                name: 'Nemesis',
            },
            {
                name: 'Poseidon',
            },
            {
                name: 'Arthur',
                spoiler: true,
            },
        ],
    },
    {
        name: 'Varatha (Eternal Spear)',
        aspects: [
            {
                name: 'Zagreus',
            },
            {
                name: 'Achilles',
            },
            {
                name: 'Hades',
            },
            {
                name: 'Guan Yu',
                spoiler: true,
            },
        ],
    },
    {
        name: 'Aegis (Shield of Chaos)',
        aspects: [
            {
                name: 'Zagreus',
            },
            {
                name: 'Chaos',
            },
            {
                name: 'Zeus',
            },
            {
                name: 'Beowulf',
                spoiler: true,
            },
        ],
    },
    {
        name: 'Coronacht (Heart-Seeking Bow)',
        aspects: [
            {
                name: 'Zagreus',
            },
            {
                name: 'Chiron',
            },
            {
                name: 'Hera',
            },
            {
                name: 'Rama',
                spoiler: true,
            },
        ],
    },
    {
        name: 'Malphon (Twin Fists)',
        aspects: [
            {
                name: 'Zagreus',
            },
            {
                name: 'Talos',
            },
            {
                name: 'Demeter',
            },
            {
                name: 'Gilgamesh',
                spoiler: true,
            },
        ],
    },
    {
        name: 'Exagryph (Adamant Rail)',
        aspects: [
            {
                name: 'Zagreus',
            },
            {
                name: 'Eris',
            },
            {
                name: 'Hestia',
            },
            {
                name: 'Lucifer',
                spoiler: true,
            },
        ],
    },
]
const getRandomOption = require('../../../utils/get-random-option')
const utils = require('../../race/race-utils')
const saveRace = require('../../../db/save-race')

module.exports = async (message, args) => {
    let weaponCriteria = () => true
    if (args.includes('--nospoiler')) {
        weaponCriteria = aspect => !aspect.spoiler
    }
    const randomWeapon = getRandomOption(WEAPONS)
    const randomAspect = getRandomOption(randomWeapon.aspects, weaponCriteria)
    const activeRace = await utils.getCurrentRace(message.channel.name)
    if (activeRace) {
        activeRace.details.metadata = Object.assign(activeRace.details.metadata || {}, {
            Weapon: randomWeapon.name,
            Aspect: randomAspect.name,
        })
        message.channel.setTopic(`${randomWeapon.name} : ${randomAspect.name}`)
        saveRace(activeRace, { details: { metadata: activeRace.details.metadata } })
    }
    return `${randomWeapon.name} - Aspect of ${randomAspect.name}`
}
