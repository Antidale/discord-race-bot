const checkMessage = require('../../../utils/check-message')
const flagBuilder = require('../utils/flagstring-builder')
const buildUserFlags = require('../utils/build-user-flags')
const getRandomFlags = require('../utils/get-random-flags')
const FEFlagSpec = require('./FEFlagSpec')

const RACE_WORTHY = {
    73: {
        Cvanilla: false,
    },
    141: {
        Etoggle: true,
        Evanilla: false,
        Ereduce: false,
        Enoencounters: false,
        Edanger: false,
    },
    146: {
        Ecantrun: false,
    },
    147: {
        Enoexp: false,
    },
    167: {
        '-vintage': false,
    },
    65: {
        Kvanilla: false,
        Kmain: true,
    },
    115: {
        Tempty: false,
    },
    124: {
        Sempty: false,
    },
    166: {
        '-vanilla:z': false,
    },
    164: {
        '-vanilla:fashion': false,
    },
    163: {
        '-vanilla:exp': false,
    },
    133: {
        Bvanilla: false,
        Bstandard: true,
    },
}
const makeFlagsRaceable = (selectedFlags) => {
    const updatedFlags = selectedFlags
    Object.keys(RACE_WORTHY).forEach((flagGroup) => {
        Object.keys(RACE_WORTHY[flagGroup]).forEach((flag) => {
            updatedFlags[flagGroup][flag] = RACE_WORTHY[flagGroup][flag]
        })
    })
    // Remove 'only this character' flags
    for (let i = 84; i <= 95; i += 1) {
        Object.keys(selectedFlags[i]).forEach((flag) => { updatedFlags[i][flag] = false })
    }
    return updatedFlags
}
const setMaxObjectives = (selectedFlags, maxObjectives) => {
    const updatedFlags = selectedFlags
    const selectedRandomObjs = Object.keys(selectedFlags['60']).find(v => selectedFlags['60'][v] === true)
    const randomObjs = selectedRandomObjs ? Number(selectedRandomObjs.replace('Orandom:', '')) : 0
    // Hardcoded nonsense
    let objsFromModes = 0
    if (selectedFlags['0']['Omode:classicforge'] === true) { objsFromModes += 1 }
    if (selectedFlags['1']['Omode:classicgiant'] === true) { objsFromModes += 1 }
    if (selectedFlags['2']['Omode:fiends'] === true) { objsFromModes += 5 }
    if (selectedFlags['3']['Omode:dkmatter'] === true) { objsFromModes += 1 }
    let currentObjs = randomObjs + 8 + objsFromModes
    if (currentObjs > maxObjectives) {
        // Objective 1 = 5 (Add 7 for each other obj)
        for (let i = 53; i >= 4; i -= 7) {
            Object.keys(selectedFlags[i]).forEach((j) => {
                updatedFlags[i][j] = false
            })
            currentObjs -= 1
            if (currentObjs <= maxObjectives) {
                i = 0
            }
        }
    }
    return updatedFlags
}
const createRandomFlags = (userFlags, isRaceable, maxObjectives) => {
    const flagSpecObj = FEFlagSpec.FlagSpec
    const uniqueFlags = flagSpecObj.binary

    const flagGroups = { }
    uniqueFlags.forEach((flagData) => {
        const { offset, flag } = flagData
        if (flagGroups[offset] && flagGroups[offset].unique) {
            flagGroups[offset].unique.push({ group: offset, flag })
        } else {
            flagGroups[offset] = { unique: [{ group: offset, flag }] }
        }
    })
    let selectedFlags = getRandomFlags(flagGroups, userFlags)
    if (isRaceable) {
        selectedFlags = makeFlagsRaceable(selectedFlags)
    }
    if (maxObjectives) {
        selectedFlags = setMaxObjectives(selectedFlags, maxObjectives)
    }
    const flagString = flagBuilder(selectedFlags)
    return flagString
}
module.exports = async (message, args) => {
    if (!await checkMessage(message)) { return null }
    const returnBin = args.includes('--binary')
    const updatedArgs = args.filter(arg => arg !== '--binary')
    const userFlags = buildUserFlags(updatedArgs)
    const isRaceable = args.includes('--raceable')
    const maxObjectives = args.includes('--maxobjectives') && args[args.indexOf('--maxobjectives') + 1]
    let flagString = createRandomFlags(userFlags, isRaceable, maxObjectives)
    const flagset = new FEFlagSpec.FlagSet()
    flagset.load(flagString)
    const flagLogic = FEFlagSpec.logic
    let flagErrors = flagLogic.fix(flagset)
    const MAX_TRIES = 10
    let tryCount = 0
    while (flagErrors.find(err => err[0] === 'error') && tryCount < MAX_TRIES) {
        flagString = createRandomFlags(userFlags, isRaceable, maxObjectives)
        flagset.load(flagString)
        flagErrors = flagLogic.fix(flagset)
        tryCount += 1
    }
    if (tryCount >= MAX_TRIES) {
        return 'Failed to generate a valid set of flags, please try again.'
    }
    return returnBin ? flagset.to_binary() : flagset.to_string(false)
}
