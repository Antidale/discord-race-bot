const ff4flags = require('./ff4flags')
const randomFlags = require('./random-flags')
const ff4beta = require('./ff4beta')

module.exports = {
    '!ff4flags': ff4flags,
    '!ff4random': randomFlags,
    '!ff4beta': ff4beta,
}
