
module.exports = (randomizedFlags, forcedFlags) => {
    let flagObj = forcedFlags || { }
    Object.keys(randomizedFlags).forEach((key) => {
        const flags = randomizedFlags[key]
        Object.keys(flags).forEach((flag) => {
            if (flags[flag] === false) {
                delete flags[flag]
            }
        })
        flagObj = Object.assign(flagObj, flags)
    })
    const flagStringBuilder = { }
    Object.keys(flagObj).forEach((key) => {
        const flagKey = key[0] === '-' ? key : key[0]
        const flagOptions = key[0] === '-' ? key : key.slice(1)
        if (flagStringBuilder[flagKey]) {
            flagStringBuilder[flagKey].push(flagOptions)
        } else {
            flagStringBuilder[flagKey] = [flagOptions]
        }
    })
    let flagString = ''
    Object.keys(flagStringBuilder).forEach((flag) => {
        const flagKey = flag.length > 1 ? '' : flag
        flagString += `${flagKey}${flagStringBuilder[flag].join('/')} `
    })
    return flagString
}
