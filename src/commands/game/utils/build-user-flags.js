module.exports = (args) => {
    const userFlags = []
    if (args && args.length > 0) {
        args.forEach((arg) => {
            const flagGroup = arg[0] === '-' ? arg : arg[0]
            if (flagGroup !== arg) {
                const flags = arg.split('/')
                for (let i = 0; i < flags.length; i += 1) {
                    const flag = flags[i]
                    const flagObj = { group: flagGroup, flag: `${flag}` }
                    userFlags.push(flagObj)
                }
            } else {
                userFlags.push({ group: flagGroup, flag: flagGroup })
            }
        })
    }
    return userFlags
}
