const utils = require('./race-utils')
const saveEntrant = require('../../db/race/save-entrant')
const findUser = require('../../utils/user/find-user')
const securityUtils = require('../../utils/security/security-utils')

module.exports = async (message, args, hasAdmin) => {
    if (!hasAdmin) {
        return 'You are unauthorized to use this command.'
    }
    if (!message.guild) {
        return 'This command cannot be used outside of a guild.'
    }
    if (!args || args.length === 0 || args.length % 2 !== 0) {
        return 'Invalid arguments.'
    }
    const raceName = message.channel.name.replace('-spoilers', '')
    const race = await utils.getCurrentRace(raceName)
    if (!race) {
        return 'There is no ongoing race for this channel.'
    }
    const securityResult = securityUtils.verifyRaceMessage(message, race)
    if (securityResult) {
        return securityResult
    }
    const entrantArgIndex = args.indexOf('--entrant')
    if (entrantArgIndex >= 0) {
        const entrantName = args[entrantArgIndex + 1]
        const fieldName = args[entrantArgIndex + 2]
        const fieldValue = args[entrantArgIndex + 3]
        if (!entrantName || !fieldName || !fieldValue) {
            return 'Invalid arguments.'
        }
        const user = await findUser(entrantName, message.guild)
        const entrant = await utils.findEntrant(race, user)
        if (!entrant) {
            return `No entrant named ${entrantName} was found`
        }
        entrant[fieldName] = fieldValue
        await saveEntrant(race, entrant)
        utils.checkIfRaceIsOver(message.channel, race)
        return `${entrantName}'s ${fieldName} has been updated to ${fieldValue}`
    }
    return null
}
