const moment = require('moment')
const utils = require('./race-utils')
const updateRaceMessages = require('../../utils/race/update-race-messages')
const determineTime = require('../../utils/determine-time')
const determineTimeByDuration = require('../../utils/determine-time-by-duration')
const CONSTANTS = require('../../constants')
const logger = require('../../utils/logger')
const getUserDisplayName = require('../../utils/user/get-user-display-name')
const getUserDetails = require('../../utils/user/get-user-details')
const grantAccessToSpoilerChannels = require('../../utils/race/grant-access-to-spoiler-channels')
const getActiveRaceForUser = require('../../utils/user/get-active-race-for-user')
const addEntrantToRace = require('../../db/race/add-entrant-to-race')
const addFinisherToRace = require('../../db/race/add-finisher-to-race')
const saveEntrant = require('../../db/race/save-entrant')
const sendDirectMessage = require('../../utils/send-direct-message')
const securityUtils = require('../../utils/security/security-utils')

const getCorrectSuffix = (placement) => {
    if (placement % 10 === 1 && placement !== 11) {
        return 'st'
    } if (placement % 10 === 2 && placement !== 12) {
        return 'nd'
    } if (placement % 10 === 3 && placement !== 13) {
        return 'rd'
    }
    return 'th'
}
const determinePlacement = (race, entrant) => {
    let placement = 1
    const updatedRace = race
    if (updatedRace.details.finishers && updatedRace.details.finishers.length > 0) {
        const sortedFinishers = updatedRace.details.finishers.sort((a, b) => moment.duration(a.finish).asMilliseconds() - moment.duration(b.finish).asMilliseconds())
        if (entrant.finishTime === sortedFinishers[sortedFinishers.length - 1].finishTime) {
            placement = sortedFinishers[sortedFinishers.length - 1].placement
        } else {
            placement = sortedFinishers.length + 1
        }
    }
    return placement
}
const updateRaceForEntrant = (race, entrant, finish, finishTime) => {
    const updatedEntrant = entrant
    logger.logInfo(`${entrant.name} has finished at ${finish} / ${finishTime}`)
    updatedEntrant.status = CONSTANTS.ENTRANT_STATUS.FINISHED
    updatedEntrant.finishTime = finishTime
    updatedEntrant.finish = finish
    updatedEntrant.placement = determinePlacement(race, updatedEntrant)
    return updatedEntrant.placement
}
const processTeamFinish = (entrant, race, channel) => {
    // Check if there's another person on the same team that has finished, if so display the combined time.
    const otherTeamMembers = race.details.entrants.filter((ent => ent.team === entrant.team && ent.id !== entrant.id))
    if (otherTeamMembers && otherTeamMembers.length > 0 && !otherTeamMembers.find(ent => ent.status === CONSTANTS.ENTRANT_STATUS.RUNNING)) {
        const teamname = entrant.team
        const allTimes = otherTeamMembers.map(ent => ent.finish)
        allTimes.push(entrant.finish)
        const sumTimes = allTimes.reduce((total, time) => total + moment.duration(time, 'HH:mm:ss.SSS').asMilliseconds(), 0)
        const teamTime = determineTimeByDuration(moment.duration(sumTimes))
        setImmediate(() => {
            channel.send(`Team ${teamname} has finished with times of ${allTimes.join(', ')} with a combined time of ${teamTime}`)
        })
    }
}
const finishSyncRace = async (message, race, user, channel) => {
    let result = null
    const finishTime = moment(message.createdTimestamp).toISOString(true)
    const botFinish = moment().toISOString(true)
    const finish = determineTime(race.details.startTime, finishTime)
    const entrant = utils.findEntrant(race, user)
    if (!entrant) { return `${user.username} is not currently in this race.` }
    if (entrant.status === CONSTANTS.ENTRANT_STATUS.FINISHED) {
        return 'You have already submitted a time for this race.'
    }
    const placement = updateRaceForEntrant(race, entrant, finish, finishTime)
    entrant.botFinish = botFinish
    await saveEntrant(race, entrant)
    await addFinisherToRace(race, entrant)
    result = `${user.username} has finished the race in ${placement}${getCorrectSuffix(placement)} place with a time of ${finish}`
    processTeamFinish(entrant, race, channel)
    await utils.checkIfRaceIsOver(channel, race)
    updateRaceMessages(message.client, race)
    channel.send(result)
    return null
}
const finishAsyncRace = async (message, args, race, user) => {
    // Set up to delete the message immediately after processing
    setImmediate(() => message.delete())
    if (!args || args.length === 0) {
        sendDirectMessage(message.author, 'You must specify a finish time when you finish an async race.  Usage: `!done 00:00:00`')
        return null
    }
    const updatedRace = race
    const [finish] = args
    const finishTime = moment().toISOString(true)
    if (!finish.match(/^[0-9]+:[0-9]{2}:[0-9]{2}$/)) {
        sendDirectMessage(message.author, 'Incorrectly formatted finish time.  Please use hh:mm:ss format.  Example: `01:23:45`')
        return null
    }
    if (!moment(finish, 'hh:mm:ss').isValid()) {
        sendDirectMessage(message.author, 'Incorrectly formatted finish time.  Please use hh:mm:ss format.  Example: `01:23:45`')
        return null
    }
    let entrant = utils.findEntrant(updatedRace, user)
    if (entrant && entrant.status === CONSTANTS.ENTRANT_STATUS.FINISHED) {
        sendDirectMessage(message.author, 'You have already submitted a time for this race.')
        return null
    }
    const userDetails = await getUserDetails(message)
    const displayName = await getUserDisplayName(userDetails, message.guild)
    const teamIndex = args.indexOf('--team')
    const teamName = teamIndex >= 0 ? args[teamIndex + 1] : displayName
    if (!entrant) {
        entrant = {
            name: displayName, status: CONSTANTS.ENTRANT_STATUS.FINISHED, id: message.author.id, team: teamName,
        }
        await addEntrantToRace(race, entrant)
    } else {
        entrant.status = CONSTANTS.ENTRANT_STATUS.FINISHED
        if (teamName !== displayName) {
            entrant.team = teamName
        }
    }
    updateRaceForEntrant(race, entrant, finish, finishTime)
    await saveEntrant(race, entrant)
    updateRaceMessages(message.client, race)
    grantAccessToSpoilerChannels(message, race)
    if (race.details.spoilerChannels.length > 0) {
        race.details.spoilerChannels.forEach(async (ch) => {
            const spoilerChannel = message.guild.channels.resolve(ch)
            if (spoilerChannel) {
                const doneMessage = await spoilerChannel.send(`${user.username} has finished the race with a time of ${finish}.`)
                doneMessage.pin()
                processTeamFinish(entrant, race, spoilerChannel)
            }
        })
    }
    return null
}
const processRaceFinish = async (message, args, race, user, channel) => {
    let result = null
    if (!race.details.async) {
        result = await finishSyncRace(message, race, user, channel)
    } else {
        result = await finishAsyncRace(message, args, race, user)
    }
    return result
}
const finishRace = async (message, args) => {
    let { channel } = message
    const user = message.author
    let race = null
    if (!message.guild) {
        race = await getActiveRaceForUser(user)
        if (!race) {
            return 'You do not currently have a race active that can be finished.'
        }
        const guild = await message.client.guilds.fetch(race.details.guild.id)
        if (!guild) {
            return 'Unable to determine the server this race is running on.  Please consult an admin.'
        }
        channel = guild.channels.cache.find(ch => ch.name === race.key)
        if (!channel) {
            return 'No channel was found for the race.  Please consult an admin.'
        }
    } else {
        race = await utils.getCurrentRace(message.channel.name)
    }

    if (!race) { return null }
    if (message.guild && !securityUtils.isMessageOnRaceServer(message, race)) { return `You must send this message on ${race.details.guild.name}.` }
    let result = null
    if (race.details.status !== CONSTANTS.RACE_STATUS.RUNNING) {
        result = 'You cannot finish this race, as it has not started or has already finished.'
        message.delete()
    } else {
        result = await processRaceFinish(message, args, race, user, channel)
    }
    return result
}
module.exports = finishRace
