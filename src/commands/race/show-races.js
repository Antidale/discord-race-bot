const racedata = require('./race-data')
const getConfigValue = require('../../utils/get-config-value')
const sendDirectMessage = require('../../utils/send-direct-message')
const displayTable = require('../../utils/table-layout')
const CONSTANTS = require('../../constants')
const sortRaces = require('./sort-races')

const showRaces = async (message) => {
    if (message.channel.name !== await getConfigValue(message, CONSTANTS.SERVER_CONFIG.MAIN_BOT_CHANNEL, CONSTANTS.CONFIG.BOT_CHANNEL)) { return }
    racedata.currentRaces().then((allRaces) => {
        let filteredRaces = allRaces
        if (message.guild) {
            filteredRaces = filteredRaces.filter(race => race.details.guild.id === message.guild.id)
        }
        let textValue = '```\n'
        if (filteredRaces.length > 20) {
            filteredRaces = filteredRaces.sort(sortRaces)
            filteredRaces.splice(0, 20)
            textValue += 'Only the most recent 20 results are shown.\n'
        }
        const headers = ['Name', 'Server', 'Status', 'Game', 'Create Time', 'Entrants', 'Async?']
        const rows = filteredRaces.map(race => ({
            name: race.key,
            server: race.details.guild.name || 'Unknown',
            status: race.details.status,
            game: race.details.type[0],
            created: race.details.created,
            entrants: race.details.entrants.length,
            async: race.details.async,
        }))
        textValue += displayTable(headers, rows)
        textValue += '```'
        sendDirectMessage(message.author, textValue)
        if (message.guild) {
            message.delete()
        }
    })
}
module.exports = showRaces
