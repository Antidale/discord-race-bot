const moment = require('moment')

const sortRaces = (race1, race2) => moment.utc(race2.details.created).diff(moment.utc(race1.details.created))
module.exports = sortRaces
