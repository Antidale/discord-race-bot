const checkMessage = require('../../utils/check-message')
const raceUtils = require('./race-utils')
const saveRace = require('../../db/save-race')
const securityUtils = require('../../utils/security/security-utils')

const MAX_KEY_LENGTH = 75
const MAX_VALUE_LENGTH = 1024

module.exports = async (message, args, hasAdmin) => {
    if (!checkMessage(message, { checkRace: true, checkArgs: true })) { return null }
    if (!args || args.length < 2) {
        return 'Invalid arguments specified.'
    }
    const currentRace = await raceUtils.getCurrentRace(message.channel.name)
    if (!securityUtils.isSentByCreatorOrAdmin(message, currentRace, hasAdmin)) { return null }
    // No need to check currentRace since it's already checked for in checkMessage
    const securityResult = securityUtils.verifyRaceMessage(message, currentRace)
    if (securityResult) {
        return securityResult
    }
    const [metaType, metaValue] = args
    const scrubbedMetaType = metaType.replace(/\s/g, '')
    if (!currentRace.details.metadata) {
        currentRace.details.metadata = { }
    }
    if (scrubbedMetaType.length > MAX_KEY_LENGTH) {
        return `Key length exceeded the maximum value of ${MAX_KEY_LENGTH}. Please try a smaller key.`
    }
    if (metaValue.length > MAX_VALUE_LENGTH) {
        return `Value length exceeded the maximum value of ${MAX_VALUE_LENGTH}. Please try a smaller value.`
    }
    currentRace.details.metadata[scrubbedMetaType] = metaValue
    saveRace(currentRace, { details: { metadata: currentRace.details.metadata } })
    return `${scrubbedMetaType} has been set to ${metaValue}`
}
