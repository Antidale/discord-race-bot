const securityUtils = require('../../utils/security/security-utils')
const getRace = require('../../db/race/get-race-by-key')
const saveRace = require('../../db/save-race')
const saveEntrant = require('../../db/race/save-entrant')
const CONSTANTS = require('../../constants')
const updateRaceMessages = require('../../utils/race/update-race-messages')

module.exports = async (message, args, hasAdmin) => {
    const race = await getRace(message.channel.name)
    if (!race) { return 'There is no race in this room.' }
    if (!race.details.async) { return 'This race is not async and cannot be started as one.' }
    const securityResult = securityUtils.verifyRaceMessage(message, race)
    if (securityResult) {
        return securityResult
    }
    if (!securityUtils.isSentByCreatorOrAdmin(message, race, hasAdmin)) {
        return `You do not have permission to start the race. Only ${race.details.creator.name} or an admin may start this race.`
    }
    if (race.details.status !== CONSTANTS.RACE_STATUS.OPEN) {
        return 'This race has already been started.  Please use `!finishasync` to finalize the race.'
    }
    race.details.status = CONSTANTS.RACE_STATUS.RUNNING
    race.details.entrants.forEach((entrant) => {
        const updatedEntrant = entrant
        updatedEntrant.status = CONSTANTS.ENTRANT_STATUS.RUNNING
        saveEntrant(race, entrant)
    })
    await updateRaceMessages(message.client, race)
    await saveRace(race, { details: { status: race.details.status } })
    return 'The race has been started.  Please use `!finishasync` to finalize the race.'
}
