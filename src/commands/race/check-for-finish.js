const utils = require('./race-utils')
const CONSTANTS = require('../../constants')

module.exports = async (message, args, hasAdmin) => {
    if (!hasAdmin) {
        return 'Only admins may use this command'
    }
    const race = await utils.getCurrentRace(message.channel.name)
    if (!race) {
        return 'There is no race in this channel.'
    }
    if (race.details.status === CONSTANTS.RACE_STATUS.CANCELED || race.details.status === CONSTANTS.RACE_STATUS.FINISHED) {
        return `This race is already ${race.details.status}`
    }
    await utils.checkIfRaceIsOver(message.channel, race)
    return 'Checked for race finish...'
}
