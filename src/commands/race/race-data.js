const getAllRaces = require('../../db/race/get-races')

const getActiveRaces = async () => {
    const activeRaces = await getAllRaces({ active: true })
    return activeRaces
}
const getFinishedRaces = async () => {
    const finishedRaces = await getAllRaces({ completed: true })
    return finishedRaces
}
module.exports = {
    currentRaces: getActiveRaces,
    finishedRaces: getFinishedRaces,
    allRaces: getAllRaces,
}
