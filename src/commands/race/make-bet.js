const getUserDetails = require('../../utils/user/get-user-details')
const saveEntrant = require('../../db/race/save-entrant')
const utils = require('./race-utils')
const securityUtils = require('../../utils/security/security-utils')
const isRaceEditable = require('../../utils/race/is-race-editable')

module.exports = async (message, args) => {
    if (!args || args.length === 0) {
        return 'You must enter an amount to bet.'
    }
    const [amount, betTarget] = args
    const wagerAmount = Number.parseInt(amount, 10)
    if (wagerAmount <= 0 || Number.isNaN(wagerAmount)) {
        return 'You must enter a positive amount to bet.'
    }
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace) { return null }
    const securityResult = securityUtils.verifyRaceMessage(message, currentRace)
    if (securityResult) {
        return securityResult
    }
    if (currentRace.details.async) {
        return 'You cannot bet cookies on async races.'
    }
    if (!isRaceEditable(currentRace)) {
        return 'It is too late to bet cookies on this race.  Cookies must be bet while the race is in the Open status'
    }

    const entrant = utils.findEntrant(currentRace, betTarget)
    if (!entrant) {
        return `${betTarget} is not currently in this race.`
    }
    const userDetails = await getUserDetails(message)
    // TODO: Update bet array in the race
    if (wagerAmount > userDetails.cookies) {
        return `You cannot wager ${wagerAmount} as you only have ${userDetails.cookies} to wager!`
    }
    saveEntrant(currentRace, entrant)
    return `${userDetails.name} has wagered ${wagerAmount} cookies on ${entrant.name}!`
}
