const moment = require('moment')
const utils = require('./race-utils')
const saveEntrant = require('../../db/race/save-entrant')
const CONSTANTS = require('../../constants')
const grantAccessToSpoilerChannels = require('../../utils/race/grant-access-to-spoiler-channels')
const securityUtils = require('../../utils/security/security-utils')
const getUserDisplayName = require('../../utils/user/get-user-display-name')
const getUserDetails = require('../../utils/user/get-user-details')
const addEntrantToRace = require('../../db/race/add-entrant-to-race')

const forfeitRace = async (message, args) => {
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace) { return null }
    const securityResult = securityUtils.verifyRaceMessage(message, currentRace)
    if (securityResult) {
        return securityResult
    }
    let result = null
    let entrant = utils.findEntrant(currentRace, message.author)
    if (!entrant && !currentRace.details.async) {
        return `${message.author.username} is not currently in this race.`
    }
    if (currentRace.details.status !== CONSTANTS.RACE_STATUS.RUNNING) {
        return `You cannot forfeit from a race in the ${currentRace.details.status} state.`
    }
    if (!entrant) {
        const userDetails = await getUserDetails(message)
        const displayName = await getUserDisplayName(userDetails, message.guild)
        const teamIndex = args.indexOf('--team')
        const teamName = teamIndex >= 0 ? args[teamIndex + 1] : displayName
        entrant = {
            name: displayName, status: CONSTANTS.ENTRANT_STATUS.RUNNING, id: message.author.id, team: teamName,
        }
        await addEntrantToRace(currentRace, entrant)
    }
    if (entrant.status === CONSTANTS.ENTRANT_STATUS.FORFEIT) {
        return `${message.author.username} has already forfeited this race.`
    }
    if (entrant.status !== CONSTANTS.ENTRANT_STATUS.RUNNING) {
        return `${message.author.username} cannot forfeit a race because they are in the ${entrant.status} status.`
    }
    entrant.status = CONSTANTS.ENTRANT_STATUS.FORFEIT
    entrant.finishTime = moment().toISOString(true)
    entrant.finish = CONSTANTS.ENTRANT_STATUS.FORFEIT
    result = `${message.author.username} has forfeited from the race.`
    grantAccessToSpoilerChannels(message, currentRace)
    saveEntrant(currentRace, entrant)
    utils.checkIfRaceIsOver(message.channel, currentRace)
    return result
}
module.exports = forfeitRace
