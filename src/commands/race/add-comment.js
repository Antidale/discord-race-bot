const saveEntrant = require('../../db/race/save-entrant')
const utils = require('./race-utils')
const securityUtils = require('../../utils/security/security-utils')
const CONSTANTS = require('../../constants')

const addComment = async (message, args) => {
    if (!args) { return 'You did not leave a comment!' }
    const comment = args.join(' ')
    let race = await utils.getCurrentRace(message.channel.name)
    if (!race) {
        // Attempt to get a race if it's the spoiler channel
        if (message.channel.name.includes('-spoilers')) {
            const key = message.channel.name.replace('-spoilers', '')
            race = await utils.getCurrentRace(key)
            if (!race) { return null }
        }
    }
    const securityResult = securityUtils.verifyRaceMessage(message, race)
    if (securityResult) {
        return securityResult
    }
    const entrant = await utils.findEntrant(race, message.author)
    if (!entrant) { return 'You cannot comment on a race you are not in!' }
    let wasDeleted = false
    if (race.details.status !== CONSTANTS.RACE_STATUS.FINISHED) {
        wasDeleted = true
        message.delete()
    }
    entrant.comment = comment
    saveEntrant(race, entrant)
    return `Comment added! ${wasDeleted ? '(Message was deleted since the race is still ongoing!)' : ''}`
}
module.exports = addComment
