const getUserDetails = require('../../utils/user/get-user-details')
const saveEntrant = require('../../db/race/save-entrant')
const utils = require('./race-utils')
const securityUtils = require('../../utils/security/security-utils')
const isRaceStartable = require('../../utils/race/is-race-startable')

module.exports = async (message, args) => {
    if (!args || args.length === 0) {
        return 'You must enter an amount to wager.'
    }
    let wagerAmount = Number.parseInt(args[0], 10)
    if (wagerAmount <= 0 || Number.isNaN(wagerAmount)) {
        return 'You must enter a positive amount to wager.'
    }
    const currentRace = await utils.getCurrentRace(message.channel.name)
    if (!currentRace) { return null }
    const securityResult = securityUtils.verifyRaceMessage(message, currentRace)
    if (securityResult) {
        return securityResult
    }
    if (currentRace.details.async) {
        return 'You cannot wager cookies on async races.'
    }
    if (!isRaceStartable(currentRace)) {
        return 'It is too late to wager cookies on this race.  Cookies must be wagered while the race is in the Open status'
    }
    const entrant = utils.findEntrant(currentRace, message.author)
    if (!entrant) {
        return 'You are not currently in this race.'
    }
    const userDetails = await getUserDetails(message)
    if (entrant.wager) {
        wagerAmount = entrant.wager + wagerAmount
    }
    if (wagerAmount > userDetails.cookies) {
        return `You cannot wager ${wagerAmount} as you only have ${userDetails.cookies} to wager!`
    }
    entrant.wager = wagerAmount
    saveEntrant(currentRace, entrant)
    const totalWagered = currentRace.details.entrants.reduce((total, ent) => total + (ent.wager || 0), 0)
    return `${entrant.name} has wagered ${wagerAmount} total cookies on this race!  There are now a total of ${totalWagered} up for grabs!`
}
