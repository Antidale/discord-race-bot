const saveRace = require('../../db/save-race')
const CONSTANTS = require('../../constants')
const getRaceByKey = require('../../db/race/get-race-by-key')
const updateRaceMessages = require('../../utils/race/update-race-messages')

module.exports = async (message, args, hasAdmin) => {
    if (!message.guild) { return null }
    const race = await getRaceByKey(message.channel.name)
    if (!race) {
        return 'There is no race happening in this channel.'
    }
    const hasPermission = (hasAdmin || race.details.creator.id === message.author.id)
    if (hasPermission) {
        if (race.details.status === CONSTANTS.RACE_STATUS.OPEN) {
            return 'The race is already unlocked.'
        }
        if (race.details.status !== CONSTANTS.RACE_STATUS.LOCKED) {
            return `You cannot unlock the race in the ${race.details.status} status.`
        }
        race.details.status = CONSTANTS.RACE_STATUS.OPEN
        updateRaceMessages(message.client, race)
        await saveRace(race, { details: { status: race.details.status } })
    } else {
        return 'You do not have the proper permissions for this command.'
    }
    return 'The race has been unlocked.'
}
