const getConfigValue = require('../../utils/get-config-value')
const CONSTANTS = require('../../constants')
const logger = require('../../utils/logger')

module.exports = async (message) => {
    const { member } = message
    if (!member) {
        return 'This command can only be used on an active server of which you are a member.'
    }
    const raceAlertRoleName = await getConfigValue(message, CONSTANTS.SERVER_CONFIG.RACE_ALERT_ROLE, null)
    if (!raceAlertRoleName) {
        return 'No race alert role has been configured for this server.'
    }
    const raceAlertRole = message.guild.roles.cache.find(role => role.name === raceAlertRoleName)
    if (!raceAlertRole) {
        return `No role named ${raceAlertRoleName} was found. Please check with an admin to make sure the server is configured correctly.`
    }
    if (member.roles.cache.find(rar => rar.id === raceAlertRole.id)) {
        try {
            const result = await member.roles.remove(raceAlertRole)
            if (result) {
                return `${message.author.username} has been successfully removed from the ${raceAlertRoleName} role.`
            }
        } catch (e) {
            logger.error(e)
            return 'An error has occurred while updated roles. Please check that the bot has permissions'
        }
    }
    try {
        const result = await member.roles.add(raceAlertRole)
        if (result) {
            return `${message.author.username} has been successfully added to the ${raceAlertRoleName} role.`
        }
    } catch (e) {
        logger.error(e)
        return 'An error has occurred while updating roles. Please check that the bot has permissions'
    }
    return null // Should not actually happen
}
