const saveUserData = require('../../db/user/save-user')
const getUserDetails = require('../../utils/user/get-user-details')

const MAX_DISPLAY_NAME = 30
module.exports = async (message, args) => {
    if (!args || args.length === 0) {
        return 'A name must be provided.'
    }
    if (!message.guild) {
        return 'This command must be sent from a server of which you are a member.'
    }
    if (!message.guild.member) {
        return 'You must be a member of this server to choose a display name.'
    }
    const newDisplayName = args.join(' ')
    if (newDisplayName.replace(' ', '') === '') {
        return 'A name must be provided.'
    }
    if (newDisplayName.length > MAX_DISPLAY_NAME) {
        return `Please choose a name less than ${MAX_DISPLAY_NAME} in length.`
    }
    const userDetails = await getUserDetails(message)
    if (!userDetails.guildDetails || Object.keys(userDetails.guildDetails).length === 0) {
        await saveUserData(userDetails, { guildDetails: { } })
    }
    const guildDetails = userDetails.guildDetails[message.guild.id] || { }
    guildDetails.display_name = newDisplayName
    const updatedDetails = { guildDetails: { } }
    updatedDetails.guildDetails[message.guild.id] = guildDetails
    await saveUserData(userDetails, { guildDetails: updatedDetails })
    return `You shall now be called ${newDisplayName}`
}
