const getUserDetails = require('../../utils/user/get-user-details')
const updateCookies = require('../../db/user/update-cookies')
const getActiveRace = require('../../utils/user/get-active-race-for-user')
const getRandomNumber = require('../../utils/get-random-number')

const { cookieReactions } = require('../../../data/cookies.json')

module.exports = async (message) => {
    const userInfo = await getUserDetails(message)
    if (userInfo.cookies <= 0) {
        return 'You have no cookies to eat!'
    }
    const activeRace = await getActiveRace(userInfo)
    if (activeRace && activeRace.details.entrants.some(ent => ent.id === userInfo.id && ent.wager > 0)) {
        return 'You may not eat your cookies while actively wagering them.'
    }
    updateCookies(userInfo, -1, '!eatcookie', 'Ate a cookie')
    const option = cookieReactions[getRandomNumber(0, cookieReactions.length)]
    return `You ate a :cookie: ! ${option}`
}
