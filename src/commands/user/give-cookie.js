const getUserDetails = require('../../utils/user/get-user-details')
const findUser = require('../../utils/user/find-user')
const updateCookies = require('../../db/user/update-cookies')
const getActiveRace = require('../../utils/user/get-active-race-for-user')
const getUserDisplayName = require('../../utils/user/get-user-display-name')

module.exports = async (message, args) => {
    const userInfo = await getUserDetails(message)
    if (userInfo.cookies <= 0) {
        return 'You have no cookies to give!'
    }
    if (!args || args.length === 0) {
        return 'You must choose who to give a cookie to!'
    }
    let cookieAmount = 1
    if (args.length === 2) {
        const argValue = Number(args[1])
        if (!Number.isInteger(argValue)) {
            return 'Invalid amount specified. Please choose a positive integer value.'
        }
        if (argValue <= 0) {
            return 'Invalid amount specified. Please choose a positive integer value.'
        }
        cookieAmount = argValue
    }
    const recipient = await findUser(args[0], message.guild)
    if (!recipient) {
        return `No user with the name ${args[0]} was found!`
    }
    if (userInfo.id === recipient.id) {
        return 'You cannot give yourself a cookie!'
    }
    const activeRace = await getActiveRace(userInfo)
    if (activeRace && activeRace.details.entrants.some(ent => ent.id === userInfo.id && ent.wager > 0)) {
        return 'You may not give away your cookies while actively wagering them.'
    }
    if (userInfo.cookies < cookieAmount) {
        return `You only have ${userInfo.cookies}, you cannot give away ${cookieAmount}...`
    }
    updateCookies(userInfo, -cookieAmount, '!givecookie', `Gave cookie(s) to ${recipient.name}(${recipient.id})`)
    updateCookies(recipient, cookieAmount, '!givecookie', `Received cookie(s) from ${userInfo.name}(${userInfo.id})`)
    const giverDisplayName = await getUserDisplayName(userInfo, message.guild)
    const recipientDisplayName = await getUserDisplayName(recipient, message.guild)
    return `${giverDisplayName} gave ${cookieAmount === 1 ? 'a cookie' : `${cookieAmount} cookies`} to ${recipientDisplayName}! ${recipientDisplayName} now has ${recipient.cookies + cookieAmount} cookies! YUM! :cookie: ` // eslint-disable-line
}
