const getUserDetails = require('../../utils/user/get-user-details')
const saveUser = require('../../db/user/save-user')

module.exports = async (message) => {
    const user = await getUserDetails(message)
    if (!user) {
        return 'User not found.'
    }
    user.race_details.personal_best = null
    user.race_details.personal_best_race = null
    saveUser(user, { race_details: { personal_best: null, personal_best_race: null } })
    return 'Your personal best details have been reset.'
}
