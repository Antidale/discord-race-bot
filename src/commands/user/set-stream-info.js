const getUserDetails = require('../../utils/user/get-user-details')
const saveUserData = require('../../db/user/save-user')

const TWITCH_BASE = 'https://twitch.tv/'
module.exports = async (message, args) => {
    if (!args || args.length === 0) {
        return 'No stream information was provided!'
    }
    const userDetails = await getUserDetails(message)
    const scrubbed = args[0].replace('https://', '').replace('http://', '').replace('twitch.tv/', '').replace('www.', '')
    userDetails.streamInfo = `${TWITCH_BASE}${scrubbed}`
    await saveUserData(userDetails, { streamInfo: userDetails.streamInfo })
    return `${userDetails.name}'s stream has been set to <${userDetails.streamInfo}>`
}
