const racedata = require('../race/race-data')
const sendDirectMessage = require('../../utils/send-direct-message')
const sortRaces = require('../race/sort-races')
const displayTable = require('../../utils/table-layout')

const MAX_RACES = 10
const parseFilters = require('../../utils/stats/parse-filters')
const applyFilters = require('../../utils/stats/apply-filters')

module.exports = async (message, args) => {
    let allRaces = await racedata.allRaces()
    const filters = parseFilters(args)
    if (filters && filters.length > 0) {
        allRaces = applyFilters(allRaces, filters)
    }
    if (message.guild) {
        allRaces = allRaces.filter((item) => {
            if (item.details.guild && item.details.guild.id) {
                return item.details.guild.id === message.guild.id
            }
            return item.details.guild === message.guild.id
        })
    }
    let textValue = '```\n'
    allRaces.sort(sortRaces)
    if (allRaces.length > MAX_RACES) {
        allRaces = allRaces.slice(0, MAX_RACES)
        textValue += `Only the most recent ${MAX_RACES} results are shown.\n`
    }
    const headers = ['Name', 'Status', 'Game', 'Create Time', 'Finish Time']
    const rows = allRaces.map(race => ({
        key: race.key,
        status: race.details.status,
        game: race.details.type[0],
        created: race.details.created,
        finished: race.details.finishTime,
    }))
    textValue += displayTable(headers, rows)
    textValue += '\n```'
    sendDirectMessage(message.author, textValue)
    if (message.guild) {
        message.delete()
    }
}
