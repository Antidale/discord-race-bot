#!/bin/bash

set -e
eval $(ssh-agent -s)

mkdir -p ~/.ssh
DIR=$(pwd)
echo "Creating SSH Key..."
echo -e "$PRIVATE_KEY" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa

echo "Creating SSH Config"
echo -e "Host *\n\tStrictHostKeyChecking no" > ~/.ssh/config

DEPLOY_SERVER=$DEPLOY_SERVER

echo "Deploying to server $DEPLOY_SERVER"
cd ~/.ssh
ssh -i "id_rsa" ec2-user@$DEPLOY_SERVER 'bash' < $DIR/deploy/updateAndRestart.sh
echo "Completed deploying to $DEPLOY_SERVER"