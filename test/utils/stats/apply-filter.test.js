const test = require('tape')
const testDataset = require('../../data/finished_races.test.json').races
const applyFilter = require('../../../src/utils/stats/apply-filter')

test('It should apply a single filter with case insensitivity', (t) => {
    let filteredSet = applyFilter(testDataset, { name: '--game', value: 'ff4fe' })
    t.is(filteredSet.length, 36, 'The game filter was correctly applied.')

    filteredSet = applyFilter(testDataset, { name: '--start', value: '2018-12-01T02:00:00.000Z' })
    t.is(filteredSet.length, 58, 'The start filter was applied correctly.')

    filteredSet = applyFilter(testDataset, { name: '--end', value: '2019-01-01T02:00:00.000Z' })
    t.is(filteredSet.length, 83, 'The end filter was applied correctly.')

    filteredSet = applyFilter(testDataset, { name: '--numentrants', value: 10 })
    t.is(filteredSet.length, 11, 'The numentrants filter was applied correctly.')

    filteredSet = applyFilter(testDataset, {
        name: '--ff4flags',
        value: 'V1 Jia Kqmt! Pk C -rescue -notellah -nofusoya -hobs T4r S3 B -whichburn F Nc Gl Et Xsb -aa -spoon -fab -huh -z',
    })
    t.is(filteredSet.length, 1, 'The ff4flags filter was applied correctly.')

    filteredSet = applyFilter(testDataset, { name: '--entrant', value: 'Supremacy' })
    t.is(filteredSet.length, 27, 'The entrant filter was applied correctly with case-sensitive.')

    filteredSet = applyFilter(testDataset, { name: '--entrant', value: 'supRemAcy' })
    t.is(filteredSet.length, 27, 'The entrant filter was applied correctly with case-insensitive.')

    filteredSet = applyFilter(testDataset, { name: '--team', value: 'CryStal-ForgE' })
    t.is(filteredSet.length, 26, 'The team filter was applied correctly with case-insensitive.')

    filteredSet = applyFilter(testDataset, { name: '--team', value: 'Crystal-Forge' })
    t.is(filteredSet.length, 26, 'The team filter was applied correctly with case-sensitive.')

    filteredSet = applyFilter(testDataset, { name: '--roller', value: 'SchalaKitty' })
    t.is(filteredSet.length, 22, 'The roller filter was applied correctly with case-sensitive.')

    filteredSet = applyFilter(testDataset, { name: '--roller', value: 'schalakiTTy' })
    t.is(filteredSet.length, 22, 'The roller filter was applied correctly with case-insensitive.')

    t.end()
})
