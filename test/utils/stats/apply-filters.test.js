const test = require('tape')
const testDataset = require('../../data/finished_races.test.json').races
const applyFilters = require('../../../src/utils/stats/apply-filters')

test('Function returns when no filters are input', (t) => {
    t.same(applyFilters(testDataset, null), testDataset, 'The data was returned unfiltered when null was passed in.')
    t.same(applyFilters(testDataset), testDataset, 'The data was returned unfiltered when nothing was passed in.')
    t.same(applyFilters(testDataset, []), testDataset, 'The data was returned unfiltered when empty array was passed in.')
    t.end()
})
test('Function applies multiple filters when they are passed in', (t) => {
    const filteredData = applyFilters(testDataset, [
        { name: '--entrant', value: 'Supremacy' },
        { name: '--entrant', value: 'Invenerable' },
        { name: '--roller', value: 'SchalaKitty' },
        { name: '--numentrants', value: 6 },
        { name: '--start', value: '2018-09-01' },
        { name: '--end', value: '2019-01-01' },
        { name: '--team', value: 'crystal-forge' },
    ])
    t.is(filteredData.length, 4, 'The rows were correctly filtered.')
    t.end()
})
