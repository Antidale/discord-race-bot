const test = require('tape')
const proxyquire = require('proxyquire')
const userDataStub = require('../../data/user_details.test.json')

const mockDataRetriever = (name, guild) => {
    const nameLower = name.toLowerCase()
    const findByName = userDataStub.find(u => u.name.toLowerCase() === nameLower)
    if (findByName) {
        return findByName
    }
    const findByNickname = userDataStub.find(u => u.guildDetails && u.guildDetails[guild] && u.guildDetails[guild].nickname && u.guildDetails[guild].nickname.toLowerCase() === nameLower)
    if (findByNickname) {
        return findByNickname
    }
    const findByDisplayName = userDataStub.find(u => u.guildDetails && u.guildDetails[guild] && u.guildDetails[guild].display_name && u.guildDetails[guild].display_name.toLowerCase() === nameLower)
    if (findByDisplayName) {
        return findByDisplayName
    }
    return null
}
const getUserData = async (name, guild) => mockDataRetriever(name, guild)

const findUser = proxyquire('../../../src/utils/user/find-user', { '../../db/user/get-user-by-name': getUserData })

test('It should return null if there is nothing to match on', async (t) => {
    t.is(await findUser(null, null), null, 'Null is returned if the user and guild are null.')
    t.is(await findUser(null, { }), null, 'Null is returned if the user is null and the guild is not.')
    t.end()
})
test('It should find a user with a match on the username', async (t) => {
    t.is((await findUser('Supremacy', { })).name, 'Supremacy', 'The user is found by the exact username.')
    t.is((await findUser('supremacy', { })).name, 'Supremacy', 'The user is found by case-insensitive username.')
    t.is((await findUser('Margaret Ann', { })).name, 'Margaret Ann', 'The user is found with spaces in the name.')
    t.end()
})
test('It should find a match on the display name with the appropriate guild', async (t) => {
    t.is((await findUser('Santapremacy', { id: '411615349579186178' })).name, 'Supremacy', 'The user is found by the display name on the given guild.')
    t.is((await findUser('santapremacy', { id: '411615349579186178' })).name, 'Supremacy', 'The user is found by the display name on the given guild with case insensitivity.')
    t.is(await findUser('Santapremacy', { }), null, 'The user is not found by the display name without a guild.')
    t.end()
})
