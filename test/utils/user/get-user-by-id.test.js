const test = require('tape')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

const rowByIdMock = sinon.fake()
const getUserById = proxyquire('../../../src/db/user/get-user-by-id', { '../util/get-row-by-id': rowByIdMock })

test('Get User By Id calls the common function to select a user by id', async (t) => {
    await getUserById('abc')
    t.is(rowByIdMock.callCount, 1, 'The common function was called.')
    t.is(rowByIdMock.getCall(0).args[0], 'users', 'The users table was called.')
    t.is(rowByIdMock.getCall(0).args[1], 'abc', 'The id was passed to the function.')
    t.end()
})
