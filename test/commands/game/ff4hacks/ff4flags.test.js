const test = require('tape')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

const sendMessageStub = sinon.fake()
const raceUtilsStub = {
    getCurrentRace: sinon.fake(),
}
const randomStub = {
    generate: () => 'TESTSEED',
}
const saveStub = sinon.fake()
const fetchFake = sinon.fake()
const fetchStub = async (url) => {
    fetchFake(url)
    return {
        json: () => ({
            status: 'exists',
            seed_id: 'test',
        }),
    }
}
process.env.FE_API_KEY = 'test'

const ff4flags = proxyquire('../../../../src/commands/game/ff4hacks/ff4flags',
    {
        'node-fetch': fetchStub,
        '../../../utils/send-message': sendMessageStub,
        '../../race/race-utils': raceUtilsStub,
        '../../../db/save-race': saveStub,
        randomstring: randomStub,
    })

test('FF4Flags returns error messages if no flags are given', async (t) => {
    t.is(await ff4flags({ }, null), 'No flags were given.', 'An error message is returned if no flags are input.')
    t.is(await ff4flags({ }, []), 'No flags were given.', 'An error message is returned if no flags are input.')
    t.is(saveStub.callCount, 0, 'Nothing was called to save.')
    t.is(fetchFake.callCount, 0, 'Fetch was not called when no flags were given.')
    t.end()
})
test('FF4Flags attempts to generate an existing seed with the given flags', async (t) => {
    t.is(await ff4flags({ channel: { name: 'Test' } }, ['S4']), 'Seed generation pending...', 'A pending message was returned.')
    // Wait a bit to make sure the promise chain gets called all the way
    setTimeout(() => {
        t.is(fetchFake.callCount, 2, 'Fetch was properly called.')
        t.is(fetchFake.firstCall.args[0], 'http://ff4fe.com/api/generate?key=test&flags=S4&seed=TESTSEED', 'The URL was properly formed.')
        t.is(fetchFake.getCall(1).args[0], 'http://ff4fe.com/api/seed?key=test&id=test', 'The seed URL was properly formed.')
        t.end()
    }, 200)
})
