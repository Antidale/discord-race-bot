const test = require('tape')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

const utilsMock = { }
const securityStub = sinon.fake()
securityStub.verifyRaceMessage = () => null
let saveMock = sinon.fake()
test('It should return early if requirements are not met', async (t) => {
    let checkMock = () => false
    saveMock = sinon.fake()
    let setMetadataValue = proxyquire('../../../src/commands/race/set-metadata-value', {
        './race-utils': utilsMock,
        '../../utils/check-message': checkMock,
        '../../db/save-race': saveMock,
        '../../utils/security/security-utils': securityStub,
    })
    const msg = { }
    t.is(await setMetadataValue(msg, ['Test1', 'Test2'], false), null, 'It returns early if the user does not have admin permissions.')
    t.is(await setMetadataValue(msg, ['Test1', 'Test2'], true), null, 'It returns early if checkMessage is false.')

    checkMock = () => true
    setMetadataValue = proxyquire('../../../src/commands/race/set-metadata-value', {
        './race-utils': utilsMock,
        '../../utils/check-message': checkMock,
        '../../db/save-race': saveMock,
        '../../utils/security/security-utils': securityStub,
    })
    t.is(await setMetadataValue(msg, null, true), 'Invalid arguments specified.', 'It returns an error if no arguments are specified.')
    t.is(await setMetadataValue(msg, [], true), 'Invalid arguments specified.', 'It returns an error if there are no args.')
    t.is(await setMetadataValue(msg, ['Test'], true), 'Invalid arguments specified.', 'It returns an error if there is only a single arg.')

    t.is(saveMock.callCount, 0, 'Save was not called during error testing scenarios.')

    t.end()
})

test('It should correctly modify the race metadata when requirements are met', async (t) => {
    const checkMock = () => true
    saveMock = sinon.fake()
    const fakeRace = {
        key: 'test',
        details: {

        },
    }
    utilsMock.getCurrentRace = () => fakeRace
    const setMetadataValue = proxyquire('../../../src/commands/race/set-metadata-value', {
        './race-utils': utilsMock,
        '../../utils/check-message': checkMock,
        '../../db/save-race': saveMock,
        '../../utils/security/security-utils': securityStub,
    })

    const msg = { channel: { name: 'test' } }

    t.is(await setMetadataValue(msg, ['Property1', 'Value1'], true), 'Property1 has been set to Value1', 'The metadata was created and the new property was stored.')
    t.is(fakeRace.details.metadata.Property1, 'Value1', 'The race metadata property was correctly set.')

    t.is(await setMetadataValue(msg, ['Property2', 'Value2'], true), 'Property2 has been set to Value2', 'The new metadata property was created.')
    t.is(fakeRace.details.metadata.Property2, 'Value2', 'The race metadata property was correctly set.')

    t.is(await setMetadataValue(msg, ['Property2', 'Value3'], true), 'Property2 has been set to Value3', 'The existing metadata property was updated.')
    t.is(fakeRace.details.metadata.Property2, 'Value3', 'The race metadata property was correctly set.')

    t.is(saveMock.callCount, 3, 'Save was called the correct amount of times.')

    t.end()
})
