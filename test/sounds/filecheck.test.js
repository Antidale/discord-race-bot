const test = require('tape')
const path = require('path')
const fs = require('fs')

test('The Bot-Join.mp3 file exists', async (t) => {
    const filePath = path.resolve('./src/sounds/Bot-Join.mp3')
    fs.readFile(filePath, (err, data) => {
        t.is(err, null, 'An error message was not thrown.')
        t.not(data, undefined, 'The file was not undefined.')
        t.not(data, null, 'The file was not null.')
        t.end()
    })
})
